# test get_median_slope_aspect and get_mean_slope_aspect from calculate_slope_aspect.py for all test points and visualize the results
import sys
import csv
import numpy as np
import matplotlib.pyplot as plt
from modelling.point import Point
from modelling.strategies import (
    get_median_slope_aspect,
    get_mean_slope_aspect,
    simple_slope_aspect,
    single_triangle_slope_aspect,
    combined_slope_aspect,
    get_planar_slope_aspect,
)

test_methods = {
    "combined": combined_slope_aspect,
    "median": get_median_slope_aspect,
    "mean": get_mean_slope_aspect,
    "single_triangle": single_triangle_slope_aspect,
    "simple": simple_slope_aspect,
    "planar": get_planar_slope_aspect,
}

distances = [5, 10, 15, 20]


def progress(count, total, status=""):
    bar_len = 30
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = "=" * filled_len + "-" * (bar_len - filled_len)

    sys.stdout.write("[%s] %s%s ...%s\r" % (bar, percents, "%", status))
    sys.stdout.flush()


def parse_raw_data():
    with open("test/data.csv", "r", encoding="latin-1") as f:
        reader = csv.reader(f, delimiter=";")
        data = list(reader)

        # create list of dictionaries with lat=Lat, lng=Lon, height=Seehöhe, slope=Hangneigung, aspect=Hangexposition
        test_data = []
        for row in data[2:]:
            # try except for empty rows
            try:
                # if(row[4] != "LWD Tirol"):
                #     continue
                test_data.append(
                    {
                        "lat": float(row[12]),
                        "lng": float(row[13]),
                        "height": float(row[14]),
                        "slope": float(row[15]),
                        "aspect": row[16],
                        "id": row[0],
                    }
                )
            except:
                pass

        # filter out data points with aspect="unbekannt"
        test_data = [
            data_point
            for data_point in test_data
            if data_point["aspect"] != "unbekannt"
        ]

        print(f"Anzahl der Testdaten: {len(test_data)}")

        return test_data


def distance_between_aspects(aspect1, aspect2):
    aspect_names = ["N", "NO", "O", "SO", "S", "SW", "W", "NW"]
    aspect1_index = aspect_names.index(aspect1)
    aspect2_index = aspect_names.index(aspect2)

    # aspects are a circle, so the distance between them can be calculated by the minimum of the distance clockwise and counterclockwise
    distance_clockwise = abs(aspect1_index - aspect2_index)
    distance_counterclockwise = len(aspect_names) - distance_clockwise
    distance = min(distance_clockwise, distance_counterclockwise)

    return distance


def test(point, distance=10, method=get_mean_slope_aspect):
    # get relative error of slope, aspect and height for a given point and distance using the given method
    base_point = Point(point["lat"], point["lng"])
    base_point.request_height()

    test_result = method(base_point, distance)

    result = {}
    result["slope_error"] = (
        100 * abs(test_result["slope"] - point["slope"]) / point["slope"]
    )
    result["aspect_error"] = (
        100 * distance_between_aspects(test_result["aspect_name"], point["aspect"]) / 8
    )
    result["height_error"] = (
        100 * abs(test_result["height"] - point["height"]) / point["height"]
    )
    return result


def test_all(test_data):
    slope_errors = {}
    aspect_errors = {}
    height_errors = {}

    for i, point in enumerate(test_data):
        # test for methods in test_methods
        # for distance 10
        for method_name, method in test_methods.items():
            # save test result in slope_errors, aspect_errors and height_errors at key method_name
            test_result = test(point, distance=10, method=method)

            if method_name not in slope_errors:
                slope_errors[method_name] = []
            if method_name not in aspect_errors:
                aspect_errors[method_name] = []
            if method_name not in height_errors:
                height_errors[method_name] = []
            slope_errors[method_name].append(test_result["slope_error"])
            aspect_errors[method_name].append(test_result["aspect_error"])
            height_errors[method_name].append(test_result["height_error"])

        progress(
            i + 1, len(test_data), status=f"Test {i + 1} of {len(test_data)} completed."
        )

    return {
        "slope_errors": slope_errors,
        "aspect_errors": aspect_errors,
        "height_errors": height_errors,
    }


def visualize(errors):
    # plot above in one figure
    plt.figure()
    # subplot at first row
    plt.subplot(2, 1, 1)
    # stacked column diagram for height, slope and aspect errors for each method in test_methods

    # calculate mean error for each method
    mean_height_errors = {}
    mean_slope_errors = {}
    mean_aspect_errors = {}
    for method_name, method in test_methods.items():
        mean_height_errors[method_name] = np.mean(errors["height_errors"][method_name])
        mean_slope_errors[method_name] = np.mean(errors["slope_errors"][method_name])
        mean_aspect_errors[method_name] = np.mean(errors["aspect_errors"][method_name])

    # create stacked bar width mean height error, mean slope error and mean aspect error for each method, method name on x axis and relative error of slope, aspect and height stacked as bar on y axis
    # use same colors
    for i, method_name in enumerate(test_methods.keys()):
        plt.bar(i, mean_height_errors[method_name], color="#2176B0", label="height")
        plt.bar(
            i,
            mean_slope_errors[method_name],
            bottom=mean_height_errors[method_name],
            color="#FD800C",
            label="slope",
        )
        plt.bar(
            i,
            mean_aspect_errors[method_name],
            bottom=mean_height_errors[method_name] + mean_slope_errors[method_name],
            color="#2BA02D",
            label="aspect",
        )

    plt.xticks(np.arange(len(test_methods)), test_methods.keys())

    # get max error for y axis
    max_error = (
        max(mean_height_errors.values())
        + max(mean_slope_errors.values())
        + max(mean_aspect_errors.values())
    )
    plt.yticks(np.arange(0, max_error, 2))
    plt.ylabel("relative error [%]")
    # add grid lines for y axis
    plt.grid(axis="y")
    plt.title("Mean error for different methods")

    # add legend for stacked bar, filter out duplicate labels
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = dict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(), loc="lower left")

    # subplot at second row, first col
    plt.subplot(2, 2, 3)
    # boxplot for slope errors for each method
    values = []
    for method_name, method in test_methods.items():
        values.append(errors["slope_errors"][method_name])
    plt.boxplot(values)
    # x ticks for method names
    plt.xticks(range(1, len(test_methods) + 1), test_methods.keys())
    plt.ylabel("relative slope error [%]")
    plt.title("Slope error for different methods")

    plt.subplot(2, 2, 4)
    # boxplot for aspect errors for each method
    values = []
    for method_name, method in test_methods.items():
        values.append(errors["aspect_errors"][method_name])
    plt.boxplot(values)
    # x ticks for method names
    plt.xticks(range(1, len(test_methods) + 1), test_methods.keys())
    plt.ylabel("relative aspect error [%]")
    plt.title("Aspect error for different methods")

    figure = plt.gcf()  # get current figure
    figure.set_size_inches(10, 8)
    plt.savefig("test/output/mean_slope_aspect_error.png", dpi=100)
    plt.show()


if __name__ == "__main__":
    test_data = parse_raw_data()

    # test_all_distances(test_data)
    errors = test_all(test_data)
    visualize(errors)
