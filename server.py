# create a simple web server using flask
# it should listen on port 80
# the path / with the parameters lat, lng and timestamp should be passed to main.py, and
# the response should be returned to the client

# flask documentation: https://flask.palletsprojects.com/en/1.1.x/quickstart/

from flask import Flask, request
from main import get_data
import datetime

app = Flask(__name__)


@app.route("/")
def index():
    lat = float(request.args.get("lat"))
    lng = float(request.args.get("lng"))
    timestamp = request.args.get("timestamp")
    timestamp = datetime.datetime.strptime(timestamp, "%Y-%m-%d %H:%M")
    # check if argument aspect is given or height or both are given, execute get_data with the given arguments
    if request.args.get("aspect") and request.args.get("height"):
        aspect = request.args.get("aspect")
        height = float(request.args.get("height"))
        data = get_data(lat, lng, timestamp, 10, aspect=aspect, height=height)
    elif request.args.get("aspect"):
        aspect = request.args.get("aspect")
        data = get_data(lat, lng, timestamp, 10, aspect=aspect)
    elif request.args.get("height"):
        height = float(request.args.get("height"))
        data = get_data(lat, lng, timestamp, 10, height=height)
    else:
        data = get_data(lat, lng, timestamp, 10)

    return data


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
