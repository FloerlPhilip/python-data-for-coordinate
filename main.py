# create a command line interface
# parameters:
# --lat (required): the latitude of the base point
# --lng (required): the longitude of the base point
# -d --distance (required): the distance between the points in the grid

import argparse
import numpy as np
from modelling.strategies import get_mean_slope_aspect
from modelling.point import Point
import datetime
from modelling.track import Track

from bulletin import get_micro_region, get_micro_region_name, Bulletin
import time
import sys

class FunctionTimer:
    def __init__(self):
        self.function_times = {}
    
    def measure_duration(self, function, *args, **kwargs):
        start_time = time.time()
        result = function(*args, **kwargs)
        end_time = time.time()
        duration = end_time - start_time
        return result, duration
    
    def run_function(self, function_name, function, *args, **kwargs):
        result, duration = self.measure_duration(function, *args, **kwargs)
        
        if function_name not in self.function_times:
            self.function_times[function_name] = []
        
        self.function_times[function_name].append(duration)
        
        return result
    
    def print_mean_durations(self):
        mean_durations = [
            f"{function_name}: {sum(durations) / len(durations):.3f}"
            for function_name, durations in self.function_times.items()
        ]
        print("\r" + ", ".join(mean_durations), end="\n")
    
    def flush_console(self):
        sys.stdout.flush()
        sys.stderr.flush()
    
timer = FunctionTimer()


def get_data(lat, lng, timestamp, distance, aspect=None, height=None):
    base_point = Point(lat, lng)
    timer.run_function("Request Height", base_point.request_height)
    grid_distance = distance

    if grid_distance < 5:
        return {
            "error": "The distance between the points in the grid must be at least 5 meters."
        }
    else:
        region_id = timer.run_function("Get Microregion", get_micro_region, base_point)

        if region_id == None:
            data = timer.run_function("Calculate", get_mean_slope_aspect, base_point, grid_distance)
        else:
            data = timer.run_function("Calculate", get_mean_slope_aspect, base_point, grid_distance)
            data["micro_region"] = {
                "id": region_id,
                "name": get_micro_region_name(region_id)
            }
            aspect = aspect or data["aspect_name"].replace("O", "E")
            height = height or data["height"]
            
            bulletin = Bulletin(timestamp, region_id, height, aspect)
            # timer.print_mean_durations()
            timer.flush_console()
            try:
                bulletin_data = bulletin.get_data()
                bulletin_data["slope"] = data["slope"]
                return bulletin_data
                # return data
            except:
                return data


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Calculate the slope and aspect of a grid of points around a base point."
    )
    parser.add_argument(
        "-f",
        "--file",
        type=str,
        help="The path to the gpx file.",
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        help="The path to the output file.",
    )
    parser.add_argument(
        "--lat", type=float, help="The latitude of the base point."
    )
    parser.add_argument(
        "--lng", type=float, help="The longitude of the base point."
    )
    parser.add_argument(
        "-d",
        "--distance",
        default=10,
        type=int,
        help="The distance between the points in the grid.",
    )
    parser.add_argument(
        "-a",
        "--aspect",
        type=str,
        help="The aspect of the slope.",
    )
    parser.add_argument(
        "-t",
        "--time",
        default=datetime.datetime.now(),
        type=lambda s: datetime.datetime.strptime(s, "%Y-%m-%d %H:%M"),
        help='The date and time of the bulletin in format "YYYY-MM-DD HH:MM"',
    )
    parser.add_argument(
        "-e",
        "--height",
        type=int,
        help="The height of the slope.",
    )

    args = parser.parse_args()

    # if date and time are not given, display warning that current date and time will be used with values
    if args.time == datetime.datetime.now():
        # print warning with value of date and time in format "YYYY-MM-DD HH:MM"
        print(
            "Warning: current date and time will be used. The current date and time is: "
            + args.time.strftime("%Y-%m-%d %H:%M")
        )

    # ensure that file and output are given together or lat, lng are given together
    # else throw error
    if args.file and args.output:
        # if aspect, lat, lng or height are given, show warning that they will be ignored

        if args.aspect or args.lat or args.lng or args.height:
            print(
                "Warning: aspect, lat, lng and height will be ignored when using file and output."
            )
        
        # check if file is found and if it is a gpx file, if not throw error
        try:
            with open(args.file, "r") as f:
                if args.file[-4:] != ".gpx":
                    print("Error: file must be a gpx file.")
                else:
                    pass
        except FileNotFoundError:
            # throw error
            print("Error: file not found.")

        track = Track(args.file, radius = 4)
        points = track.grid_points
        data = []
        # create simple progress bar to show progress
        # print("Calculating...")
        for point in points:
            data.append(get_data(point.lat, point.lng, args.time, args.distance))
            # add point.lat and point.lng to data
            data[-1]["lat"] = point.lat
            data[-1]["lng"] = point.lng
            # print(
            #     "\rProgress: [{0:50s}] {1:.1f}%".format(
            #         "#" * int((points.index(point) / len(points)) * 50),
            #         (points.index(point) / len(points)) * 100,
            #     ),
            #     end="",
            # )

        # plot data on map
        # create map with folium
        import folium

        # create map with folium
        m = folium.Map(
            location=[points[0].lat, points[0].lng],
            zoom_start=15,
            tiles="Stamen Terrain",
        )

        # add marker to map for each point, with popup showing slope and aspect and bulletin info and color based on aspect
        # display as small circle
        for point in data:
            if point["aspect_name"] == "N":
                color = "blue"
            elif point["aspect_name"] == "NE":
                color = "green"
            elif point["aspect_name"] == "E":
                color = "yellow"
            elif point["aspect_name"] == "SE":
                color = "orange"
            elif point["aspect_name"] == "S":
                color = "red"
            elif point["aspect_name"] == "SW":
                color = "purple"
            elif point["aspect_name"] == "W":
                color = "black"
            elif point["aspect_name"] == "NW":
                color = "gray"
            folium.CircleMarker(
                location=[point["lat"], point["lng"]],
                radius=2,
                popup=folium.Popup(
                    "Slope: "
                    + str(point["slope"])
                    + "<br>Aspect: "
                    + point["aspect_name"]
                    + "<br>Region: "
                    + str(point["region_id"])
                    + "<br>Height: "
                    + str(point["height"]),
                    max_width=200,
                ),
                color=color,
                fill=True,
                fill_color=color,
            ).add_to(m)

        # save map to html file
        m.save(args.output)

        # save data to json file        
        
        with open("out.json", "w") as f:
            f.write(str(data))


    elif args.lat and args.lng:
        # call get_data with lat, lng, distance, aspect, height and time, with aspect and height as None if not given
        if args.aspect and args.height:
            data = get_data(
                args.lat,
                args.lng,
                args.time,
                args.distance,
                args.aspect,
                args.height,
            )
        else:
            data = get_data(args.lat, args.lng, args.time, args.distance)

            print(data)
