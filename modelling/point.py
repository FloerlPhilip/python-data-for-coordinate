import numpy as np
import rasterio
import pyproj
import os

EARTH_RADIUS = 6371000
CRS = 4326
DGM_CRS = 31254

#DGM_FILE_PATH = "./DTM/DGM_Tirol_5m_epsg31254_2006_2019.tif"
DGM_FILE_PATH =  os.path.join(os.path.dirname(__file__), "./DGM/DGM_Tirol_5m_epsg31254_2006_2019.tif")
DGM = rasterio.open(DGM_FILE_PATH)

def add_meters_to_coords(point, d_lat=0, d_lng=0):
    # new_lat = data_point["lat"] + distance / r_earth * (180 / np.pi)
    # new_lng = data_point["lng"] + distance / r_earth * (180 / np.pi) / np.cos(data_point["lat"] * np.pi/180)
    new_lat = point.lat + d_lat / EARTH_RADIUS * (180 / np.pi)
    new_lng = point.lng + d_lng / EARTH_RADIUS * (180 / np.pi) / np.cos(
        point.lat * np.pi / 180
    )

    return Point(new_lat, new_lng)

def get_distance_between_points(point1, point2):
    # get distance between latitudes of vetctor1  and vector2 where lat at index 0 in m
    d_lat = (point1.lat - point2.lat) * np.pi / 180 * EARTH_RADIUS

    # get distance between longitudes of vetctor1  and vector2 where lng at index 1 in m
    d_lng = (
        (point1.lng - point2.lng)
        * np.pi
        / 180
        * EARTH_RADIUS
        * np.cos((point1.lat + point2.lat) / 2 * np.pi / 180)
    )

    if point1.height is None or point2.height is None:
        d_height = 0
    else:
        d_height = point1.height - point2.height

    total = np.sqrt(d_lat**2 + d_lng**2 + d_height**2)

    return {"d_lat": d_lat, "d_lng": d_lng, "d_height": d_height, "total": total}


class Point:
    """
    Represents a point on the earth.

    ...

    Attributes
    ----------
    lat : float
        The latitude of the point.
    lng : float
        The longitude of the point.
    height : float
        The height of the point in meters above sea level.

    Methods
    -------
    convert_coordinates(src_crs, dest_crs, lat, lng)
        Converts coordinates from one CRS to another.

    fetch_height()
        Fetches the height of the point from the DGM file.

    Examples
    --------
    >>> p = Point(47.267222, 11.392778)
    >>> p.lat
    47.267222
    >>> p.lng
    11.392778
    >>> p.height
    2000.0
    """

    def __init__(self, lat, lng):
        self._lat = lat
        self._lng = lng
        self._height = None

    @property
    def lat(self):
        return self._lat

    @property
    def lng(self):
        return self._lng

    @property
    def height(self):
        return self._height

    @lat.setter
    def lat(self, value):
        self._lat = value

    @lng.setter
    def lng(self, value):
        self._lng = value

    def convert_coordinates(self, src_crs, dest_crs, lat, lng):
        transformer = pyproj.Transformer.from_crs(src_crs, dest_crs, always_xy=True)
        return transformer.transform(lng, lat)

    def request_height(self):
        x, y = self.convert_coordinates(CRS, DGM_CRS, self._lat, self._lng)

        # image pixel coordinates
        px, py = DGM.index(x, y)

        # read value of pixel at given coordinates
        try:
            height = DGM.read(1, window=((px, px + 1), (py, py + 1)))[0][0]
        except IndexError:
            raise Exception(
                f"Point {self._lat}, {self._lng} is not in the DGM file."
            )

        # round to 0 decimals
        height = int(round(height, 0))
        self._height = height

    def is_in_polygon(self, polygon):
        # check if point is in polygon
        # https://stackoverflow.com/questions/217578/how-can-i-determine-whether-a-2d-point-is-within-a-polygon
        x = self._lng
        y = self._lat

        inside = False
        for i in range(len(polygon)):
            j = i - 1
            if ((polygon[i][1] > y) != (polygon[j][1] > y)) and (
                x
                < (polygon[j][0] - polygon[i][0])
                * (y - polygon[i][1])
                / (polygon[j][1] - polygon[i][1])
                + polygon[i][0]
            ):
                inside = not inside

        return inside
