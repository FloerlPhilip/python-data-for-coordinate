# creates a list of coordinates from a gpx file, 
# create a grid with the given distance for a defined radius around
# the track and calculate the slope and aspect for each point in the grid
import matplotlib.pyplot as plt
import numpy as np
import re
import os
from modelling.point import Point, add_meters_to_coords, get_distance_between_points

class Track:
    _track_coordinates = []
    _origin = None
    _point_indices = []
    _continuous_indices = []
    _indices_around_track = []
    _grid_coordinates = []

    def __init__(self, file_path, distance=10, radius=3):
        self.file_path = file_path
        self.distance = 2 * distance
        self.radius = radius

        self._track_coordinates = self._get_points_from_file()
        self._origin = self._get_origin()
        self._point_indices = self._normalize_points()
        self._continuous_indices = self._get_continuous_indices()

        self._indices_around_track = self._get_indices_in_radius()

        # calculate the coordinates of each point in the grid
        self._grid_coordinates = self._calc_coords_from_grid()

        print("Initialization completed.")

    @property
    def track_points(self):
        return self._track_coordinates
    
    @property
    def grid_points(self):
        return self._grid_coordinates

    def _get_points_from_file(self):
        with open(self.file_path, "r") as f:
            data = f.readlines()
            points = []
            for line in data:
                # check if pattern matches lat="..." lon="..." in line
                if re.match(r".*lat=\".*\" lon=\".*\".*", line):
                    # get the lat and lng from the line
                    lat = re.search(r"lat=\"([0-9.]*)\"", line).group(1)
                    lng = re.search(r"lon=\"([0-9.]*)\"", line).group(1)
                    # create a point with the lat and lng
                    point = Point(float(lat), float(lng))
                    # add the point to the list of points
                    points.append(point)
            return points

    def _get_origin(self):
        origin = Point(90, 180)
        for point in self._track_coordinates:
            if point.lat < origin.lat:
                origin.lat = point.lat
            if point.lng < origin.lng:
                origin.lng = point.lng

        return origin

    def _get_index_from_point(self, point):
        d_lat, d_lng, height, total = get_distance_between_points(
            point, self._origin
        ).values()

        x = int(d_lat / self.distance)
        y = int(d_lng / self.distance)

        return (x, y)
    
    def _normalize_points(self):
        normalized_points = []
        for point in self._track_coordinates:
            normalized_points.append(self._get_index_from_point(point))

        # filter duplicates while maintaining order
        return list(dict.fromkeys(normalized_points))
    
    def _get_continuous_indices(self):#
        indices = []
        for i in range(len(self._point_indices) - 1):
            indices.extend(self._get_indices_between_points(self._point_indices[i], self._point_indices[i + 1]))
        
        return indices
    
    def _get_indices_between_points(self, point1, point2):
        x_diff = point2[1] - point1[1]
        y_diff = point2[0] - point1[0]

        if y_diff == 0:
            # Points are vertically aligned
            return [(point1[0], x) for x in range(min(point1[1], point2[1]), max(point1[1], point2[1]))]
                
        if x_diff == 0:
            # Points are horizontally aligned
            return [(y, point1[1]) for y in range(min(point1[0], point2[0]), max(point1[0], point2[0]))]

        reference = point1 if point1[1] < point2[1] else point2
        slope = y_diff / x_diff

        continuous_points = []
        prev_y = 0

        for x in range(1, int(abs(x_diff)) + 1):
            curr_y = int(np.ceil(abs(x * slope)))

            for y in range(prev_y, curr_y):
                offset_y = y if slope > 0 else -y
                continuous_points.append((reference[0] + offset_y, reference[1] + x))
                    
            prev_y = curr_y
                
        return continuous_points

    def _get_indices_in_radius(self):
        full_grid = []
        for point in self._continuous_indices:
            for i in range(-self.radius, self.radius + 1):
                for j in range(-self.radius, self.radius + 1):
                    full_grid.append((point[0] + i, point[1] + j))
        
        return list(dict.fromkeys(full_grid))

    def _calc_coords_from_grid(self):
        grid_coordinates = []
        for point in self._indices_around_track:
            grid_coordinates.append(add_meters_to_coords(self._origin, point[0] * self.distance, point[1] * self.distance))
        return grid_coordinates
            
    def visualize(self):
        # plot the track and the grid points
        plt.plot([point.lng for point in self._track_coordinates], [point.lat for point in self._track_coordinates], "b")
        plt.plot([point.lng for point in self._grid_coordinates], [point.lat for point in self._grid_coordinates], "ro")
        # plot point_indices
        #plt.plot([point[1] for point in self._point_indices], [point[0] for point in self._point_indices], "g-")
        plt.show()

# use above with data/largoz.gpx
if __name__ == "__main__":
    track = Track(os.path.join(os.path.dirname(__file__), "data", "largoz.gpx"))
    track.visualize()