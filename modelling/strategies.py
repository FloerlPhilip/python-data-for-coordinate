import numpy as np
from modelling.point import Point, add_meters_to_coords, get_distance_between_points
from modelling.plane import Plane


def get_grid(base_point, grid_distance):
    """
    Get a 3x3 grid of points around the base point with grid_distance spacing.
    """
    grid_offsets = [
        (grid_distance, grid_distance),
        (grid_distance, 0),
        (grid_distance, -grid_distance),
        (0, -grid_distance),
        (-grid_distance, -grid_distance),
        (-grid_distance, 0),
        (-grid_distance, grid_distance),
        (0, grid_distance),
    ]

    points = []
    for offset in grid_offsets:
        point = add_meters_to_coords(base_point, offset[0], offset[1])
        point.request_height()
        points.append(point)

    return points


def get_grid_planes(base_point, grid_distance):
    """
    Create triangles with the base point and points from the grid.
    """
    points = get_grid(base_point, grid_distance)
    num_points = len(points)

    for i in range(num_points):
        point1 = points[i]
        point2 = points[(i + 1) % num_points]
        point3 = points[(i + 2) % num_points]

        if (
            point1.lat == point2.lat == point3.lat
            or point1.lng == point2.lng == point3.lng
        ):
            continue

        yield Plane(base_point, point1, point2)


# get mean slope and aspect of a grid of points around the base point
def get_mean_slope_aspect(base_point, grid_distance):
    """
    A grid of points is created around the base point with grid_distance spacing.
    The slope and aspect of each point is calculated and the mean of all slopes and aspects is returned.

    Parameters
    ----------
    base_point : Point
        The base point around which the grid is created.
    grid_distance : int
        The distance between the points in the grid.

    Returns
    -------
    dict
        A dictionary with the mean slope and aspect of the grid.
    """
    slopes = []
    aspects = []
    for plane in get_grid_planes(base_point, grid_distance):
        slopes.append(plane.slope)
        aspects.append(plane.aspect)

    return {
        "slope": np.mean(slopes),
        "aspect": np.mean(aspects),
        "aspect_name": Plane.find_aspect_name(None, np.median(aspects)),
        "height": base_point.height,
    }



def get_median_slope_aspect(base_point, grid_distance):
    slopes = []
    aspects = []
    for plane in get_grid_planes(base_point, grid_distance):
        slopes.append(plane.slope)
        aspects.append(plane.aspect)

    return {
        "slope": np.median(slopes),
        "aspect": np.median(aspects),
        "aspect_name": plane.find_aspect_name(np.median(aspects)),
        "height": base_point.height,
    }


def simple_slope_aspect(base_point, grid_distance):
    plane = Plane(
        base_point,
        add_meters_to_coords(base_point, grid_distance, 0),
        add_meters_to_coords(base_point, 0, grid_distance),
    )
    return {
        "slope": plane.slope,
        "aspect": plane.aspect,
        "aspect_name": plane.find_aspect_name(plane.aspect),
        "height": base_point.height,
    }


def single_triangle_slope_aspect(base_point, grid_distance):
    plane = Plane(
        add_meters_to_coords(base_point, -grid_distance, -grid_distance),
        add_meters_to_coords(base_point, grid_distance, -grid_distance),
        add_meters_to_coords(base_point, 0, grid_distance),
    )
    return {
        "slope": plane.slope,
        "aspect": plane.aspect,
        "aspect_name": plane.find_aspect_name(plane.aspect),
        "height": base_point.height,
    }


def combined_slope_aspect(base_point, grid_distance):
    slope = get_mean_slope_aspect(base_point, grid_distance)["slope"]
    aspect = single_triangle_slope_aspect(base_point, grid_distance)["aspect"]
    return {
        "slope": slope,
        "aspect": aspect,
        "aspect_name": Plane.find_aspect_name(None, aspect),
        "height": base_point.height,
    }


def get_planar_slope_aspect(base_point, grid_distance):
    # implements formula from https://pro.arcgis.com/en/pro-app/latest/tool-reference/spatial-analyst/how-slope-works.htm
    points = get_grid(base_point, grid_distance)

    x_cellsize = grid_distance
    y_cellsize = grid_distance

    a, b, c, f, i, h, g, d = [point.height for point in points]

    dz_dx = ((c + 2 * f + i) - (a + 2 * d + g)) / (8 * x_cellsize)
    dz_dy = ((g + 2 * h + i) - (a + 2 * b + c)) / (8 * y_cellsize)
    slope = np.arctan(np.sqrt(dz_dx**2 + dz_dy**2)) * 180 / np.pi
    aspect = np.arctan(dz_dy / dz_dx)

    return {
        "slope": slope,
        "aspect": aspect,
        "aspect_name": Plane.find_aspect_name(None, aspect),
        "height": base_point.height,
    }


def get_linear_slope_aspect(base_point, grid_distance):
    points = get_grid(base_point, grid_distance)

    slopes = []

    for point in points:
        slope_rad = np.arctan((base_point.height - point.height) / grid_distance)
        slope = slope_rad * 180 / np.pi
        slopes.append(slope)

    aspect = single_triangle_slope_aspect(base_point, grid_distance)["aspect"]
    return {
        "slope": slope,
        "aspect": aspect,
        "aspect_name": Plane.find_aspect_name(None, aspect),
        "height": base_point.height,
    }


if __name__ == "__main__":
    p1 = Point(47.0438844, 11.2895399)
    p1.request_height()

    print(get_median_slope_aspect(p1, 10))
    print(get_mean_slope_aspect(p1, 10))
