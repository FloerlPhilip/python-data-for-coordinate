import numpy as np


class Plane:
    """
    Calculates the slope and aspect of a plane defined by three points.

    ...

    Attributes
    ----------
    aspect : float
        The aspect of the plane in degrees.
    aspect_name : str
        The aspect of the plane in cardinal directions.
    slope : float
        The slope of the plane in degrees.
    height : float
        The height of the plane in meters.
    data : dict
        A dictionary containing all the attributes of the plane.
    p1 : Point
        The first point of the plane.
    p2 : Point
        The second point of the plane.
    p3 : Point
        The third point of the plane.

    Methods
    -------
    p1(p1)
        Sets the first point of the plane.

    p2(p2)
        Sets the second point of the plane.

    p3(p3)
        Sets the third point of the plane.

    Examples
    --------
    >>> p1 = {lat: 47.123, lng: 8.123, height: 1000}
    >>> p2 = {lat: 47.123, lng: 8.124, height: 1000}
    >>> p3 = {lat: 47.124, lng: 8.123, height: 1000}
    >>> plane = Plane(p1, p2, p3)
    >>> plane.slope
    0.0
    >>> plane.aspect
    0.0
    >>> plane.aspect_name
    "N"
    >>> plane.height
    1000
    """

    def __init__(self, p1, p2, p3):
        if not p1.height:
            p1.request_height()
        if not p2.height:
            p2.request_height()
        if not p3.height:
            p3.request_height()
        self._v1 = np.array([p1.lat, p1.lng, p1.height])
        self._v2 = np.array([p2.lat, p2.lng, p2.height])
        self._v3 = np.array([p3.lat, p3.lng, p3.height])
        self._calculate_all()

    @property
    def aspect(self):
        return self._aspect

    @property
    def aspect_name(self):
        if self._slope == 0:
            return "undefined"
        return self._aspect_name

    @property
    def slope(self):
        return int(round(self._slope))

    @property
    def height(self):
        return int(round(self._height))

    @property
    def data(self):
        return {
            "aspect": self.aspect,
            "aspect_name": self.aspect_name,
            "slope": self.slope,
            "height": self.height,
        }

    @property
    def p1(self):
        return self._v1

    @property
    def p2(self):
        return self._v2

    @property
    def p3(self):
        return self._v3

    @p1.setter
    def p1(self, p1):
        self._v1 = np.array([p1.lat, p1.lng, p1.height])
        self._calculate_all()

    @p2.setter
    def p2(self, p2):
        self._v2 = np.array([p2.lat, p2.lng, p2.height])
        self._calculate_all()

    @p3.setter
    def p3(self, p3):
        self._v3 = np.array([p3.lat, p3.lng, p3.height])
        self._calculate_all()

    def _calculate_all(self):
        self._check_points_on_line(self._v1, self._v2, self._v3)
        self._normal_vector = self._get_normal_vector()
        self._slope = self._calculate_angle([0, 0, 1], self._normal_vector)
        self._aspect = self._calculate_aspect()
        self._aspect_name = self.find_aspect_name(self._aspect)
        self._height = self._v1[2]

    # function to check if the points are on the same line
    def _check_points_on_line(self, p1, p2, p3):
        # check if the points are on the same line for the x and y coordinates
        if (p1[0] == p2[0] and p2[0] == p3[0]) or (p1[1] == p2[1] and p2[1] == p3[1]):
            print(p1, p2, p3)
            raise Exception("Points are on the same line.")

    def _subtract_vectors(self, vector1, vector2):
        EARTH_RADIUS = 6371000
        # get distance between latitudes of vetctor1  and vector2 where lat at index 0 in m

        d_lat = (vector1[0] - vector2[0]) * np.pi / 180 * EARTH_RADIUS

        # get distance between longitudes of vetctor1  and vector2 where lng at index 1 in m
        d_lng = (
            (vector1[1] - vector2[1])
            * np.pi
            / 180
            * EARTH_RADIUS
            * np.cos((vector1[0] + vector2[0]) / 2 * np.pi / 180)
        )

        return np.array([d_lat, d_lng, vector1[2] - vector2[2]])

    def _get_normal_vector(self):
        r1 = self._subtract_vectors(self._v1, self._v2)
        r2 = self._subtract_vectors(self._v1, self._v3)

        normal_vector = np.cross(r1, r2)

        if normal_vector[2] < 0:
            normal_vector = -normal_vector

        return normal_vector

    def _normalize_vector(self, vector):
        return vector / np.linalg.norm(vector)

    def _rad_to_deg(self, rad):
        return rad * 180 / np.pi

    def _calculate_angle(self, vector1, vector2):
        angle_rad = np.arccos(
            np.dot(vector1, vector2)
            / (np.linalg.norm(vector1) * np.linalg.norm(vector2))
        )
        angle_deg = self._rad_to_deg(angle_rad)
        angle_deg = np.round(angle_deg, 0)
        return angle_deg

    def _calculate_aspect(self):
        normalized_normal_vector = self._normalize_vector(self._normal_vector)
        # calculate atan2 of the normal vector
        aspect_rad = np.arctan2(
            normalized_normal_vector[1], normalized_normal_vector[0]
        )
        aspect_deg = self._rad_to_deg(aspect_rad)
        aspect_deg = np.round(aspect_deg, 0)

        if aspect_deg < 0:
            aspect_deg = 360 + aspect_deg
        return aspect_deg

    def find_aspect_name(self, aspect_deg):
        aspect_names = ["N", "NO", "O", "SO", "S", "SW", "W", "NW"]
        aspect_index = int(np.round(aspect_deg / 45)) % 8
        return aspect_names[aspect_index]
