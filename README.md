# Installation:

Install required packages with pip:
    
```bash 
pip install numpy pyproj requests argparse flask
```

To install rasterio, use following commands
    
```bash
sudo add-apt-repository ppa:ubuntugis/ppa
sudo apt-get update
sudo apt-get install gdal-bin libgdal-dev
pip install -U pip
pip install rasterio
```

To install and use it as a simple web service, install flask:

```bash
pip install flask
```

Install gunicorn to run the flask server:

```bash
pip install gunicorn
```

To update the submodules, run
```bash
git submodule update --recursive --remote
```

Download the DGM for tirol via the following link, unpack it and place it in the modelling/DGM folder with name "DGM_Tirol_5m_epsg31254_2006_2019.tif":
https://gis.tirol.gv.at/ogd/geografie_planung/DGM/DGM_Tirol_5m_epsg31254.zip

# Usage:

To run the program, use following command:

```bash
python3 main.py --lat 47.2675 --lng 11.38325 -t "2023-01-01 12:00"
```

This produces following output:
```json
{'danger_level': '1', 'danger_patterns': ['DP1'], 'problem_types': [], 'height': 595, 'aspect': 'S', 'micro_region': {'id': 'AT-07-04-01', 'name': 'Westliches Karwendel'}, 'slope': 2.0}
```

If you want to run the program as a web service, use following command:

```bash
sudo python3 server.py
```

To deploy the web service with gunicron use following command:

```bash
nano /etc/systemd/system/python-data-for-coordinate.service
```

and paste following content:

```bash
[Unit]
Description=Gunicorn instance to serve python-data-for-coordinate
After=network.target

[Service]
User=python-data-for-coordinate
WorkingDirectory=/opt/python-data-for-coordinate
ExecStart=/opt/python-data-for-coordinate/venv/bin/gunicorn --bind 0.0.0.0:4142 server:app
Restart=always

[Install]
WantedBy=multi-user.target
```

Then run following commands:

```bash
sudo systemctl start python-data-for-coordinate
sudo systemctl enable python-data-for-coordinate
```

You then can make a request to the server with following parameters:

http://localhost/?lat=47.2675&lng=11.38325&timestamp=2023-01-01%2012:00
