from modelling.point import Point
import json
import requests


def get_micro_region(point: Point):
    # open micro-regions/AT-07_micro-regions.geojson.json

    with open("eaws-regions/public/micro-regions/AT-07_micro-regions.geojson.json") as f:
        data = json.load(f)

    # find micro-region for given point
    for feature in data["features"]:
        if point.is_in_polygon(feature["geometry"]["coordinates"][0][0]):
            return feature["properties"]["id"]


def get_micro_region_name(micro_region_id):
    # get micro-region name from micro-regions/micro-region_names.json
    with open("eaws-regions/public/micro-regions_names/de.json") as f:
        data = json.load(f)
        return data[micro_region_id]


# a class to download bulletin from BASE_URL + yyyy-mm-dd/yyyy-mm-dd_de_CAAMLv6_2022.json
# and parse it
# with functions to get the danger level for a given micro-region and timestamp and elevation
# and to get problem_types
class Bulletin:
    BASE_URL = "https://static.avalanche.report/bulletins/"
    DANGER_LEVEL_MAPPING = {
        "low": "1",
        "moderate": "2",
        "considerable": "3",
        "high": "4",
        "very high": "5",
    }
    PROBLEM_TYPE_MAPPING = {
        "persistent_weak_layers": "Altschnee",
        "wind_slab": "Triebschnee",
        "new_snow": "Neuschnee",
        "wet_snow": "Nassschnee",
        "gliding_snow": "Gleitschnee",
    }

    _micro_region_bulletin = None
    _danger_patterns = None
    _problem_types = None
    _danger_rating = None

    def __init__(self, timestamp, micro_region, elevation, aspect):
        self._timestamp = timestamp
        self._micro_region = micro_region
        self._elevation = elevation
        self._aspect = aspect.replace("O", "E")

    def _download_bulletin(self):
        # parse datetime to yyyy-mm-dd
        parsed_timestamp = self._timestamp.strftime("%Y-%m-%d")

        # download bulletin
        url = (
            self.BASE_URL
            + parsed_timestamp
            + "/"
            + parsed_timestamp
            + "_de_CAAMLv6.json"
        )
        
        r = requests.get(url)
        if r.status_code != 200:
            raise Exception("Could not download bulletin.")
        self._bulletin = r.json()

    def _parse_bulletin(self):
        # find bulletin for given micro-region
        for bulletin in self._bulletin["bulletins"]:
            for region in bulletin["regions"]:
                if region["regionID"] == self._micro_region:
                    self._micro_region_bulletin = bulletin
                    return self._micro_region_bulletin

    def get_danger_rating(self):
        # find danger level for given elevation
        danger_ratings = self._micro_region_bulletin["dangerRatings"]
        # add danger ratings where "validTimePeriod" matches timestamp and elevation is lower than lowerBound or higher than upperBound
        for danger_rating in danger_ratings:
            # check if validTimePeriod is "earlier" (0-12) or "later" (12-24) than timestamp or is valid all day
            if "validTimePeriod" in danger_rating:
                if (
                    danger_rating["validTimePeriod"] == "earlier"
                    and self._timestamp.hour >= 12
                ):
                    continue
                if (
                    danger_rating["validTimePeriod"] == "later"
                    and self._timestamp.hour < 12
                ):
                    continue

            # chech if property elevation exists
            if "elevation" in danger_rating:
                # check if property lowerBound exists, set lowerBound to 2000 if its "treeline", if elevation is lower than lowerBound continue
                if "lowerBound" in danger_rating["elevation"]:
                    if danger_rating["elevation"]["lowerBound"] == "treeline":
                        danger_rating["elevation"]["lowerBound"] = 2000
                    if self._elevation < int(danger_rating["elevation"]["lowerBound"]):
                        continue

                # check if property upperBound exists, set upperBound to 2000 if its "treeline", if elevation is higher than upperBound continue
                if "upperBound" in danger_rating["elevation"]:
                    if danger_rating["elevation"]["upperBound"] == "treeline":
                        danger_rating["elevation"]["upperBound"] = 2000
                    if self._elevation > int(danger_rating["elevation"]["upperBound"]):
                        continue

            self._danger_rating = danger_rating["mainValue"]
            return self._danger_rating

    def get_danger_patterns(self):
        self._danger_patterns = self._micro_region_bulletin["customData"]["LWD_Tyrol"][
            "dangerPatterns"
        ]
        return self._danger_patterns

    def get_problem_types(self):
        # make above but check if validTimePeriod is "earlier" (0-12) or "later" (12-24) than timestamp or is valid all day
        self._problem_types = []
        for avalanche_problem in self._micro_region_bulletin["avalancheProblems"]:
            # check if validTimePeriod is "earlier" (0-12) or "later" (12-24) than timestamp or is valid all day
            if "validTimePeriod" in avalanche_problem:
                if (
                    avalanche_problem["validTimePeriod"] == "earlier"
                    and self._timestamp.hour >= 12
                ):
                    continue
                if (
                    avalanche_problem["validTimePeriod"] == "later"
                    and self._timestamp.hour < 12
                ):
                    continue

            # check if elevation is in range
            if "elevation" in avalanche_problem:
                if "lowerBound" in avalanche_problem["elevation"]:
                    if avalanche_problem["elevation"]["lowerBound"] == "treeline":
                        avalanche_problem["elevation"]["lowerBound"] = 2000
                    if self._elevation < int(
                        avalanche_problem["elevation"]["lowerBound"]
                    ):
                        continue
                if "upperBound" in avalanche_problem["elevation"]:
                    if avalanche_problem["elevation"]["upperBound"] == "treeline":
                        avalanche_problem["elevation"]["upperBound"] = 2000
                    if self._elevation > int(
                        avalanche_problem["elevation"]["upperBound"]
                    ):
                        continue

            # check if aspect is in range
            if "aspects" in avalanche_problem:
                if self._aspect not in avalanche_problem["aspects"]:
                    continue

            # add problem type
            self._problem_types.append(avalanche_problem["problemType"])

    def get_data(self):
        self._download_bulletin()
        self._parse_bulletin()
        if self._micro_region_bulletin == None:
            raise Exception("Could not find bulletin for given micro-region.")
        self.get_danger_rating()
        self.get_danger_patterns()
        self.get_problem_types()

        return {
            "danger_level": self.DANGER_LEVEL_MAPPING[self._danger_rating],
            "danger_patterns": self._danger_patterns,
            "problem_types": [
                self.PROBLEM_TYPE_MAPPING[problem_type]
                for problem_type in self._problem_types
            ],
            "height": self._elevation,
            "aspect": self._aspect.replace("E", "O"),
            "micro_region": {
                "id": self._micro_region,
                "name": get_micro_region_name(self._micro_region),
            },
        }
